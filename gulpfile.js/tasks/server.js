var gulp = require('gulp'), 
    confirm = require('gulp-confirm'),
    dom = require('gulp-dom'),
    fs = require('fs'),
    path = require('path');

var adServerSrc,animationSrc,footTag,
manifestFile,
datajson,
manifestTag,
politeIndexTag,
politeDir = './ft_polite',
filePath = path.basename(path.dirname(path.dirname(__dirname)));

 /*--------------------------------------------------
Adserver code 
--------------------------------------------------*/
function doHeadTags(answer) {
     datajson = JSON.parse(fs.readFileSync('./data.json'))

    switch (answer) {
      case "1":
        // Double Click Studio
        adServerSrc = datajson.doubleClick.cdn;
        animationSrc = datajson.animationTag.jsTag;
        footTag = datajson.doubleClick.standardDC;
        break;
      case "2":

        // Double Click Studio - Polite Load
        adServerSrc = datajson.doubleClick.cdn;
        animationSrc = datajson.animationTag.jsTag;
        footTag = datajson.doubleClick.polite;
        break;
      case "3":
        // DCM 
        animationSrc = datajson.animationTag.jsTag;
        footTag = datajson.standard.adserverJs;
        break;
      case "4":
        // adServerSrc = datajson.flashTalking.cdn;
        animationSrc = datajson.animationTag.jsTag;
        footTag = datajson.standard.adserverJs;
        ftStandard();
        break;
      case "5":
        // FLASH TALKING RICH
        adServerFTSrc = datajson.flashTalking.cdn;
        animationSrc = datajson.animationTag.jsTag;
        footTag = datajson.standard.adserverJs;
        ftPolite();

        break;
      case "6":
        // polite Load js DoubleClick
        animationSrc = datajson.animationTag.jsTag;
        footTag = datajson.standard.adserverJs;
        politeJs();
        // series('mk-dc-politeLoad');
        break;
      case "7":
        // polite Load js DoubleClick
        adServerSrc = datajson.sizmek.cdn;
        footTag = datajson.sizmek.jsPolite;
        sizmekPoliteLoad()
        
        break;
      default:
        footTag = "window.onload = init();";
        break;

    }
    writeAdserver(footTag, answer);
    


  }
   
  function insertDomBundle(){
    return gulp.src('./src/index.html')
      .pipe(dom(function () {

        if (adServerSrc != undefined) {
          this.getElementById('adServerTag').setAttribute('src', adServerSrc);
        }

        if (animationSrc != undefined) {

          this.getElementById('animationTag').setAttribute('src', animationSrc);
        }

        if (filePath) {
          this.getElementsByTagName('title')[0].innerHTML = filePath;
        }
        return this;
      }))
      //

      .pipe(gulp.dest('./src/'));
      
  }
  function writeAdserver(serverTag, ans) {

    fs.writeFile('./src/js/adservers.js', serverTag, function (err) { });
    fs.writeFile('./adservers/.server.txt', ans, function (err) { });
}


// /*--------------------------------------------------
  // create Ft manifest g+ulp
  // --------------------------------------------------*/

  function ftStandard(){
    manifestTag = datajson.flashTalking.manifestStandard;

    fs.writeFile('src/manifest.js', manifestTag, function (err) { });

  }


// /*--------------------------------------------------
  // create ft polite files
  // --------------------------------------------------*/

  function ftPolite(){
    if (!fs.existsSync(politeDir)) {
      fs.mkdirSync(politeDir);
    }

    manifestTag = datajson.flashTalking.manifestRich;

    manifestTag = replaceString(manifestTag, 'filePath', filePath);

    fs.writeFile('./ft_polite/manifest.js', manifestTag, function (err) { });
  
    politeMarkup();

  }
  
  function politeMarkup(){
    if (!fs.existsSync(politeDir)) {
      fs.mkdirSync(politeDir);
    }

    politeIndexTag = datajson.flashTalking.richIndexStyle;
    return gulp.src('./adservers/ft_rich/index.html')
      .pipe(dom(function () {

        this.getElementsByTagName('style')[0].innerHTML = politeIndexTag;
        if (adServerFTSrc != undefined) {
          this.getElementById('adServerTag').setAttribute('src', adServerFTSrc);
        }
        return this;
      }))

      .pipe(gulp.dest('./ft_polite'));
   
  }

  // DoubleClick polite load load images through js

  function politeJs(){

    if (!fs.existsSync('./src/js/politeLoadAnimation.js')) {

      return gulp.src('./adservers/dc_polite/politeLoadAnimation.js')

        .pipe(gulp.dest('./src/js'));
    }
    dcPolite();


  }

  function dcPolite(){
    var fileDir = 'src/js/animation.js',
      fileExists = fs.existsSync(fileDir);
    if (animationSrc === undefined && json.version === '0.0.0') {
      if (fileExists) {
        return del(fileDir);
      }
    }
 
  }



  //sizmekPoliteLoad

  function sizmekPoliteLoad(){
    if (!fs.existsSync('./src/js/EBLoader.js')) {

      return gulp.src('./adservers/sizmek/EBLoader.js')

        .pipe(gulp.dest('./src/js'));
    }

  }



module.exports = function (done){
        json = JSON.parse(fs.readFileSync('./package.json'));
        var version = json.version.toString;
      
        if (json.version === '0.0.0') {
          gulp.src('src/**/*')
            .pipe(confirm({
              question: 'Ad Server? Press | Doubleclick: 1 | Doubleclick polite: 2 |  standard: 3 | FT standard: 4 | FT rich: 5 | Images polite: 6 | Sizmek: 7',
              proceed: function (answer) {
                doHeadTags(answer);
                return true;
              }
            }))
      
        }
        insertDomBundle();
        done();
    }



  // /*--------------------------------------------------
  // replace string with another
  // --------------------------------------------------*/
  function replaceString(target, search, replacement) {
    var str = target;
    return str.replace(new RegExp(search, 'g'), replacement);
  }
