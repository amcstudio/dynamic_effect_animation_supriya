'use strict';
~ (function() {
    var tl,
    imageContainer = document.getElementById('imageContainer'),
    mainContent = document.getElementById('mainContent');

    window.init = function() {
        revealAnimation('bg.jpg', '300')
        play();
    }

    function play(){
        var tl = gsap.timeline();
        
        tl.to('#img1',{duration:1, x:0, ease:'sine.inOut'},0);
        tl.to('#img2',{duration:1, x:-150, ease:'sine.inOut'},0)
       
    }
       

     function revealAnimation(imgName , width) {
         var left=0;
         var xPos = 150;
        for(var i = 0; i<2; i++){
            var div = document.createElement('div');
                div.id = 'img' + i;
                div.style.width= (width /2)+'px';
                div.style.overflow ="hidden"; 
                var img = document.createElement('img');
                img.src = 'img/' + imgName;  
                img.style.left=left+'px';
                img.style.transform = 'translateX('+xPos+'px)';
                img.id="img"+(i+1);
                div.appendChild(img); 
                imageContainer.appendChild(div);  
                xPos = -300;               
                     
                       
        }    
       
        

    }

}) ();
